let screenshareButton = document.getElementById("screenshare");
let canvas = document.getElementById("canvas");
let context = canvas.getContext("2d");

let bird = document.getElementById("bird");
let video = document.getElementById('video');

let isStreamingEnabled = false;

video.play();

let prediction = "loading ...";

let gradient = context.createLinearGradient(0, 0, canvas.width, 0);
gradient.addColorStop("0"," magenta");
gradient.addColorStop("0.5", "#00BEEF");
gradient.addColorStop("1.0", "#FF0000");

function RenderingLoop(videoStream)
{
    if(!isStreamingEnabled)
    {
        videoStream = bird;
    }

    context.drawImage(videoStream, 0, 0, canvas.width, canvas.height);

    // draw prediction
    context.fillStyle = gradient;
    context.font = "30px Arial";
    context.fillText(prediction, 50, 50);
}

const classifier = ml5.imageClassifier('MobileNet', modelLoaded);

function modelLoaded() {
    var imgData = context.getImageData(0, 0, canvas.width, canvas.height);
    classifier.classify(imgData, (err, results) => {
        
        if(err)
        {
            console.error(err);
        }

        prediction = results[0].label.toUpperCase() + " " + Math.round(results[0].confidence * 100) + "%";
        modelLoaded();
    });
}

screenshareButton.onclick = function()
{
    startCapture({ audio: false, video: true })
        .then((result) => 
        {
            isStreamingEnabled = true;
            video.srcObject = result;
        });
}

async function startCapture(displayMediaOptions) {
  let captureStream = null;

  try {
    captureStream = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
  } catch(err) {
    console.error("Error: " + err);
  }
  return captureStream;
}

video.addEventListener('play', function() {
    var $this = this; //cache
    (function loop() {
      if (!$this.paused && !$this.ended) {
        RenderingLoop($this);
        setTimeout(loop, 1000 / 30); // drawing at 30fps
      }
    })();
}, 0);