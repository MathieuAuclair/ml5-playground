# ML5 playground

This is just a simple project to play around with ML5 library.

Use a live server to run this project or else, you'll get CORS error in the console.

```shell
#install the live server package
npm install -g live-server

#start server
live-server
```

### Configuration (firefox)

also this playground uses autoplay which is blocked by default in firefox, got to ```settings > preferences``` then search autoplay and allow video autoplay. 